/*
Use this migration script to make data changes only. You must commit any additive schema changes first.
Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

/*SCRIPT PARA ALTA MODELO Movil*/

IF NOT EXISTS (SELECT MO_idModelo FROM MModelos WHERE MO_nombre ='MAS MOVIL')
INSERT [dbo].[MModelos] (
[MO_nombre],
[MO_bOdometro],
[MO_campoOdometro],
[MO_mensajeOdometroI],
[MO_mensajeOdometroF],
[MO_unidadOdometro],
[MO_limiteOdometro],
[MO_unidadVelocidad],
[MO_tipoReinicio],
[MO_estatus],
[MO_timeStamp],
[CT_idCuentaH],
[MO_plantillaMsj],
[MO_campoMensaje],
[MO_limiteMensaje],
[MO_prefijoMensaje],
[MO_withId],
[MO_bPMG],
[MO_tipoNivelC],
[MO_campoNivelC1],
[MO_campoNivelC2],
[MO_prefijoNivelC],
[MO_sufijoNivelC],
[MO_separadorNivelC],
[MO_uMedidaNivelC],
[MO_limTanqueVacio1],
[MO_limTanqueVacio2],
[MO_limTanqueLleno1],
[MO_limTanqueLleno2],
[MO_prefijoTemp],
[MO_separadorTemp],
[MO_uMedidaNivelC2],
[MO_uMedidaNivelC3],
[MO_limTanqueVacio3],
[MO_limTanqueLleno3],
[MO_tipoProyectoC],
[MO_campoNivelC3],
[MO_uMedidaNivelC1],
[MO_tramaUbicacion], --
[MO_tanquesDisponibles],
[MO_bSalidas],
[MO_hdopValido],
[MO_procesoExt],
[MO_campoOdometroECU],
[MO_campoNivelECU],
[MO_IOExpander]
) VALUES
(
N'Movil'
,0
,N'Odometer'
,N''
, N''
, 0
, 4294967
, 0
, 1
, N'A'
, GETDATE()
, 15554
, N'{{texto}}'
, N'Message'
, 242
, N''
, 1
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, NULL
, N'AT+GTRTO=gl300w,1,,,,,,FFFF$'
, 0
, 0
, 0
, NULL
, NULL
, NULL
, NULL
)
